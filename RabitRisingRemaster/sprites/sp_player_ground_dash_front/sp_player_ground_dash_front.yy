{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 3,
  "bbox_right": 19,
  "bbox_top": 5,
  "bbox_bottom": 21,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 22,
  "height": 22,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"051cb7a3-833c-4950-871d-62692104d890","path":"sprites/sp_player_ground_dash_front/sp_player_ground_dash_front.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"051cb7a3-833c-4950-871d-62692104d890","path":"sprites/sp_player_ground_dash_front/sp_player_ground_dash_front.yy",},"LayerId":{"name":"88033a31-3d3b-4177-8983-e009d717ce2a","path":"sprites/sp_player_ground_dash_front/sp_player_ground_dash_front.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sp_player_ground_dash_front","path":"sprites/sp_player_ground_dash_front/sp_player_ground_dash_front.yy",},"resourceVersion":"1.0","name":"051cb7a3-833c-4950-871d-62692104d890","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"31d87050-b950-4b7e-9b5b-792fac24a1cd","path":"sprites/sp_player_ground_dash_front/sp_player_ground_dash_front.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"31d87050-b950-4b7e-9b5b-792fac24a1cd","path":"sprites/sp_player_ground_dash_front/sp_player_ground_dash_front.yy",},"LayerId":{"name":"88033a31-3d3b-4177-8983-e009d717ce2a","path":"sprites/sp_player_ground_dash_front/sp_player_ground_dash_front.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sp_player_ground_dash_front","path":"sprites/sp_player_ground_dash_front/sp_player_ground_dash_front.yy",},"resourceVersion":"1.0","name":"31d87050-b950-4b7e-9b5b-792fac24a1cd","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"45881c83-85dc-447b-bf4e-219386b0c161","path":"sprites/sp_player_ground_dash_front/sp_player_ground_dash_front.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"45881c83-85dc-447b-bf4e-219386b0c161","path":"sprites/sp_player_ground_dash_front/sp_player_ground_dash_front.yy",},"LayerId":{"name":"88033a31-3d3b-4177-8983-e009d717ce2a","path":"sprites/sp_player_ground_dash_front/sp_player_ground_dash_front.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sp_player_ground_dash_front","path":"sprites/sp_player_ground_dash_front/sp_player_ground_dash_front.yy",},"resourceVersion":"1.0","name":"45881c83-85dc-447b-bf4e-219386b0c161","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"5ada7581-aff2-4072-9d91-c72123c0d0d7","path":"sprites/sp_player_ground_dash_front/sp_player_ground_dash_front.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"5ada7581-aff2-4072-9d91-c72123c0d0d7","path":"sprites/sp_player_ground_dash_front/sp_player_ground_dash_front.yy",},"LayerId":{"name":"88033a31-3d3b-4177-8983-e009d717ce2a","path":"sprites/sp_player_ground_dash_front/sp_player_ground_dash_front.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sp_player_ground_dash_front","path":"sprites/sp_player_ground_dash_front/sp_player_ground_dash_front.yy",},"resourceVersion":"1.0","name":"5ada7581-aff2-4072-9d91-c72123c0d0d7","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"26ea94b1-d262-4508-b461-775007ca5728","path":"sprites/sp_player_ground_dash_front/sp_player_ground_dash_front.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"26ea94b1-d262-4508-b461-775007ca5728","path":"sprites/sp_player_ground_dash_front/sp_player_ground_dash_front.yy",},"LayerId":{"name":"88033a31-3d3b-4177-8983-e009d717ce2a","path":"sprites/sp_player_ground_dash_front/sp_player_ground_dash_front.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sp_player_ground_dash_front","path":"sprites/sp_player_ground_dash_front/sp_player_ground_dash_front.yy",},"resourceVersion":"1.0","name":"26ea94b1-d262-4508-b461-775007ca5728","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"f8b67b19-6bf2-4392-8069-9e1fd847ba5f","path":"sprites/sp_player_ground_dash_front/sp_player_ground_dash_front.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f8b67b19-6bf2-4392-8069-9e1fd847ba5f","path":"sprites/sp_player_ground_dash_front/sp_player_ground_dash_front.yy",},"LayerId":{"name":"88033a31-3d3b-4177-8983-e009d717ce2a","path":"sprites/sp_player_ground_dash_front/sp_player_ground_dash_front.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sp_player_ground_dash_front","path":"sprites/sp_player_ground_dash_front/sp_player_ground_dash_front.yy",},"resourceVersion":"1.0","name":"f8b67b19-6bf2-4392-8069-9e1fd847ba5f","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"sp_player_ground_dash_front","path":"sprites/sp_player_ground_dash_front/sp_player_ground_dash_front.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 6.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"40c0c1e6-ee28-4a9b-a238-784e34e64b6a","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"051cb7a3-833c-4950-871d-62692104d890","path":"sprites/sp_player_ground_dash_front/sp_player_ground_dash_front.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e4a2e718-4e99-4446-9ee6-871c908a8db1","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"31d87050-b950-4b7e-9b5b-792fac24a1cd","path":"sprites/sp_player_ground_dash_front/sp_player_ground_dash_front.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"96fb96bd-1ba4-4744-ba2e-348b873a600d","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"45881c83-85dc-447b-bf4e-219386b0c161","path":"sprites/sp_player_ground_dash_front/sp_player_ground_dash_front.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"84598b86-d103-469f-8a4d-d5ba1cce16bc","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5ada7581-aff2-4072-9d91-c72123c0d0d7","path":"sprites/sp_player_ground_dash_front/sp_player_ground_dash_front.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"6ea312f9-390d-4699-a174-a181d6d1d94f","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"26ea94b1-d262-4508-b461-775007ca5728","path":"sprites/sp_player_ground_dash_front/sp_player_ground_dash_front.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f0a21018-86f1-46a5-8b20-39f57fb149aa","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f8b67b19-6bf2-4392-8069-9e1fd847ba5f","path":"sprites/sp_player_ground_dash_front/sp_player_ground_dash_front.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 11,
    "yorigin": 21,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"sp_player_ground_dash_front","path":"sprites/sp_player_ground_dash_front/sp_player_ground_dash_front.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"88033a31-3d3b-4177-8983-e009d717ce2a","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Player",
    "path": "folders/Sprites/Player.yy",
  },
  "resourceVersion": "1.0",
  "name": "sp_player_ground_dash_front",
  "tags": [],
  "resourceType": "GMSprite",
}