function player_animation(argument0) {

	/// @description player_horizontal_movement()

	switch(argument0){
	
		case player_animations.idle:
			ob_Player.sprite_index = sp_player_Idle;
		break;
	
		case player_animations.run:
			ob_Player.sprite_index = sp_player_onGround;
		break;
		
		case player_animations.jump:
			ob_Player.sprite_index = sp_player_jump;
		break;
	
		case player_animations.fall:
			ob_Player.sprite_index = sp_player_fall;
		break;
		
		case player_animations.air_dash_front:
			ob_Player.sprite_index = sp_player_air_dash_front;
		break;
	
		case player_animations.air_dash_down:
			ob_Player.sprite_index = sp_player_air_dash_down;
		break;
	
		case player_animations.ground_dash_front:
			ob_Player.sprite_index = sp_player_ground_dash_front;
		break;
	
		case player_animations.slide:
			ob_Player.sprite_index = sp_player_wall_slider;
		break;
	}


}
