/// @description player_horizontal_wall
function player_horizontal_wall() {

	// Setup Animation
	if(!isOnGround){
		player_animation(player_animations.slide);
	}

	if(ySpeed > 0 and ob_Player_Wall_Collider.player_slide_collision)ySpeed += gravityAcceleration/ 8;	
	else ySpeed += gravityAcceleration;	


#region Tile Collision
		//Horizontal Collisions
		if(tile_meeting(x + xSpeed,y ,wallLayer)){

			if(tile_meeting(x + xSpeed,y ,wallLayer)){
				while(!tile_meeting(x + sign(xSpeed),y ,wallLayer)){
					x += sign(xSpeed);
				}
			}

			xSpeed = 0;
			ySpeed =0;
		}else {
			if(last_x_direction > 0 ){
				// Check if is colliding with the wall
				if(ob_Player_Wall_Collider.player_slide_collision){
					var dust = instance_create_layer(x + random_range(-1,1) +5, y - 6, "Particles", ob_Particla_Player_Dust);
					dust.move_up = 0.2;
				}

			}else{
				// Check if is colliding with the wall
				if(ob_Player_Wall_Collider.player_slide_collision){
					var dust = instance_create_layer(x + random_range(-1,1) -5, y - 6, "Particles", ob_Particla_Player_Dust);
					dust.move_up = 0.2;	
				}
			}	
		}
#endregion

	x += xSpeed;


}
