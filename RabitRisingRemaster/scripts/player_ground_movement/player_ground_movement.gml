function player_ground_movement(){
	horizontal_movement();
	vertical_movement();
}

// Horizontal Handler
function horizontal_movement() {
	horizontal_forces();
	
	xSpeed = mov_direction * acceleration;
	xSpeed = clamp(xSpeed, -max_speed, max_speed);
	x += xSpeed;
}

function horizontal_forces() {
	if(on_left) mov_direction = 1;
	else if(on_right) mov_direction = -1;
	
	if(on_left or on_right) {
		if(!on_ground) player_state = player_states.onWall;
	}
}


// Vertical Handler
function vertical_movement() {
	vertical_forces();
	
	//Vertical Collisions
	if(on_ground) {
		if(on_ground) {
			while(!on_ground){
				y += sign(ySpeed);
			}
		}
		
		ySpeed = 0;
		
		// Jump Action
		if(jump) ySpeed = jump_height;	
	} else {
		y += ySpeed;
	}
}

function vertical_forces() {
	
	ySpeed += gravity_acceleration;
	
	if(ySpeed != 0) {
		if(ySpeed < 0) {
			gravity_acceleration = gravity_jump_acceleration;
		} else {
			gravity_acceleration = gravity_fall_acceleration;
		}
	} else {
		gravity_acceleration = gravity_jump_acceleration;
	} 
}