/// @description player_vertical_wall
function player_vertical_wall() {
#region Logic
	
		if(!ob_Player_Wall_Collider.player_slide_collision and !isOnGround){
			player_animation(player_animations.fall);
		}
#endregion

#region Object Collision
	
#endregion

#region Tile Collision

		//Slide Wall Collisions
		if(tile_meeting(x ,y + ySpeed,wallLayer)){
			while(!tile_meeting(x ,y + sign(ySpeed) ,wallLayer)){
				y += sign(ySpeed);
			}
			ySpeed = 0;
			dash_front_air_lock = true;
	
	
			//Invert movement direction
			movDirection = movDirection * -1;
			player_st = player_states.run;
		}

		//Ground Collisions
		if(tile_meeting(x ,y + ySpeed,groundLayer)){
			while(!tile_meeting(x ,y + sign(ySpeed) ,groundLayer)){
				y += sign(ySpeed);
			}
			ySpeed = 0;
			dash_front_air_lock = true;
	
	
			//Invert movement direction
			movDirection = movDirection * -1;
			player_st = player_states.run;
		} else {
			if(jump){
				//Invert movement direction
				movDirection = movDirection * -1;
				player_st = player_states.run;
				ySpeed = jumpHeight;
				player_animation(player_animations.jump);
			};	
		}

#endregion


	y += ySpeed;


}
