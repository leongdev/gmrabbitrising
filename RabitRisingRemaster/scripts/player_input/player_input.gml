// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function player_input(){
	
	if(keyboard_check_pressed(vk_space)) room_restart();
	
	//Up input
	jump_1 = keyboard_check_pressed(ord("S"));
	jump_2 = keyboard_check_pressed(vk_right);
	jump_3 = gamepad_button_check_pressed(0, gp_face1);
	jump_4 = gamepad_button_check_pressed(0, gp_shoulderr);
	jump_5 = mouse_check_button_pressed(mb_right);
	
	// Dash inout
	dash_1 = keyboard_check_pressed(ord("A"));
	dash_2 = keyboard_check_pressed(vk_left);
	dash_3 = gamepad_button_check_pressed(0,gp_face3);
	dash_4 = gamepad_button_check_pressed(0,gp_shoulderl);	
	dash_5 = mouse_check_button_pressed(mb_left);

	// Setup jump
	if(jump_1 or jump_2 or jump_3 or jump_4 or jump_5){
		canStart = true;
		jump = true;
	}else{
		jump = false;
	}

	// Setup dash
	if(dash_1 or dash_2 or dash_3 or dash_4 or dash_5){
		canStart = true;
		dash = true;
	}else{
		dash = false;
	}
}