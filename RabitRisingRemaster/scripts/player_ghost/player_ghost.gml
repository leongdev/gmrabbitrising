/// @description player_ghost()
function player_ghost() {
	//GHOST AIR DOWN
	if(!dash_down_lock){
		dash_ghost_depay += 1;

		if(dash_ghost_depay > 1){
			instance_create_layer(x,y,0,ob_Player_Air_Dash_Down_Ghost);
			dash_ghost_depay = 0;
		}
	} 

	//GHOST AIR FRONT
	if(!dash_front_air_ghost){
		dash_ghost_depay += 1;

		if(dash_ghost_depay > 1){
			instance_create_layer(x,y,0,ob_Player_Air_Dash_Front_Ghost);
			dash_ghost_depay = 0;
		}
	} 

	//GHOST GROUND FRONT
	if(!dash_front_ground_lock){
		dash_ghost_depay += 1;

		if(dash_ghost_depay > 1){
			instance_create_layer(x,y,0,ob_Player_Ground_Dash_Ghost);
			dash_ghost_depay = 0;
		}
	} 


}
