/// @description apply_friction(amount)
/// @param amount
function apply_friction(argument0) {
	var amount = argument0;

	// First check if we're moving
	if(xSpeed != 0 ){
		if(abs(xSpeed) - amount > 0){
			xSpeed -= amount * image_xscale;
		}else{
			xSpeed = 0;
		}
	}
}
