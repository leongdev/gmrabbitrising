function player_collisions() {
	on_ground = check_ground_vertical_meeting();
	on_left = check_ground_left_meeting();
	on_right = check_ground_right_meeting();
}

function check_ground_vertical_meeting() {
	if(tile_meeting(x ,y + ySpeed, ground_layer)) 
		return true;
	else 
		return false;
}

function check_ground_left_meeting() {
	if(tile_meeting(x - xSpeed ,y - 10, ground_layer)) 
		return true;
	else 
		return false;
}

function check_ground_right_meeting() {
	if(tile_meeting(x + xSpeed ,y - 10, ground_layer)) 
		return true;
	else 
		return false;
}