/// @description screen_shake(shake_length, shake_magnitude, shake_remain)
/// @param shake_length 
/// @param shake_magnitude 
/// @param shake_remain 
function screen_shake(argument0, argument1, argument2) {

	ob_Camera.shake_length = argument0;
	ob_Camera.shake_magnitude = argument1;
	ob_Camera.shake_remain = argument2;


}
