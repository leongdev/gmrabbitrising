/// @description player_onGround()
function player_onGround() {

	//Flip Player
	ob_Player.image_xscale = movDirection; 

	//Collisions
	if(canStart){
		player_horizontal_ground();
	}
	
	player_vertical_ground();

}

function player_horizontal_ground() {

	// Checking for Horizontal Movement
	if(dash_lock){
		xSpeed = movDirection * acceleration * dashForce;
	}else {
		xSpeed = movDirection * acceleration;
	}

	xSpeed = clamp(xSpeed, -maxSpeed, maxSpeed);
	
	// Update last direction
	if(xSpeed > 0 or xSpeed < 0){
		last_x_direction = xSpeed;
	}

	// Collisions
	player_general_object_horizontal_collision(ob_door_button);
	player_general_object_horizontal_collision(ob_door);
	player_general_layer_horizontal_collision(wallLayer);
	player_general_layer_horizontal_collision(groundLayer);

	x += xSpeed;
}

function player_vertical_ground() {

	// Setup gravity
	ySpeed += gravityAcceleration;	

	//Vertical Collisions
	if(tile_meeting(x ,y + ySpeed,groundLayer) or place_meeting(x,y + ySpeed, ob_door_button)){

		if(tile_meeting(x ,y + ySpeed,groundLayer)){
			while(!tile_meeting(x ,y + sign(ySpeed) ,groundLayer)){
				y += sign(ySpeed);
			}
		}

		ySpeed = 0;
		
		if(jump) ySpeed = jumpHeight;	

	} else{
		
		//if((dash)&& dash_input_lock){
		//	dash_lock = true;
		//	dash_input_lock = false;
	
		//	dash_front_air_lock = false;
		//	dash_front_air_ghost = false;
		//	alarm_set(0,dashDuration);
		//	player_animation(player_animations.air_dash_front);
		//}

		if(canStart){
			if(ySpeed > 0 and dash_down_lock and dash_front_air_lock and dash_front_ground_lock){
				player_animation(player_animations.fall);
			}else if(dash_down_lock and dash_front_air_lock and dash_front_ground_lock){
				player_animation(player_animations.jump);
			}
		}
		
		//// Check if player press DASH DOWN on air
		//if(jump && !isOnGround and canStart){
		//	player_animation(player_animations.air_dash_down);
		//	dash_down_lock = false;
		//	alarm_set(1,5);
		//}
	}

	// Setup jump forces
	y += ySpeed;

}
