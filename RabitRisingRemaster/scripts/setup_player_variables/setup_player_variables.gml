function setup_player_variables() {
	setup_types();
	setup_player_attributes();
	setup_support_attributes();
}

function setup_types() {
	enum player_states {
		onGround,
		onWall,
		onDashFront,
		onDashDown,
	}
}

function setup_player_attributes() {
	jump_height = -8;
	max_speed = 20;
	
	gravity_acceleration = 0.4;
	gravity_fall_acceleration = 1;
	gravity_jump_acceleration = 0.4;
	
	dash_force_on_air = 5;
	dash_force_on_gorund = 10;
}

function setup_support_attributes() {
	acceleration = 2;
	
	xSpeed = 0;
	ySpeed = 0;
	
	mov_direction = 1;
	player_state = player_states.onGround;
	
	jump = false;
	dash = false;
	
	ground_layer = "Level_Base";
	wall_layer = "Level_Wall_Slider";
	
	on_ground = false;
	on_left = false;
	on_right = false;
}