/// @description Insert description here
// You can write your code in this editor

x += (xTo - x)/ smooth_x;
y += (yTo - y)/ smooth_y;

if(follow != noone){
	xTo = follow.x + screen_x_offset;
	yTo = follow.y + screen_y_offset;
}

var vm = matrix_build_lookat(x,y,-10,x,y,0,0,1,0);
camera_set_view_mat(camera,vm);

//Screen shake
x += random_range(-shake_remain,shake_remain);
y += random_range(-shake_remain,shake_remain);

shake_remain = max(0, shake_remain -((1/shake_length) * shake_magnitude) );