/// @description Insert description here
// You can write your code in this editor

if(!view_enabled)
{
	view_set_wport(0,screen_width);
	view_set_hport(0,screen_height);
	view_set_visible(0,true);
	
	camera_set_view_mat(camera,vm);
	camera_set_proj_mat(camera,pm);
	
	view_camera[0] = camera;
	view_enabled = true;
}

if(window_get_width() != screen_width * cam_zoom && window_get_height() != screen_height * cam_zoom)
{
	window_set_size(screen_width * cam_zoom,screen_height*cam_zoom);
	surface_resize(application_surface,screen_width * cam_resolution, screen_height * cam_resolution);
} 