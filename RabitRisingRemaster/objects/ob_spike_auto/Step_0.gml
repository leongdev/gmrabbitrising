/// @description Insert description here
// You can write your code in this editor

switch(spike_state){
	case spike_states.idle:
		if(place_meeting(x,y,ob_Player_Pivot)){
			spike_state = spike_states.blink;
		}
		can_kill = false;
		sprite_index=sp_spike_auto_deactivated;
	break;
	case spike_states.blink:
	
		if(enter_blink){
			enter_blink = false;
			can_kill = false;
			sprite_index=sp_spike_auto_blink;
			alarm_set(0, blink_delay  * 60);
		}
			image_speed= 1;
	break;
	case spike_states.active:
		can_kill = true;
		if(enter_kill){
			enter_kill = false;
			sprite_index=sp_spike_auto_activated;
			alarm_set(1,active_delay * 60);
		}
		
		if(image_index >= image_number -1){
			image_speed= 0;
		}
	break;
}