/// @description Insert description here
// You can write your code in this editor

enum spike_states {
	idle,
	blink,
	active
}

spike_state = spike_states.idle;

enter_blink = true;
enter_kill = true;

can_kill = false;

blink_delay = 1;
active_delay = 2;