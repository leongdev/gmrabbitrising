/// @description Fade Out

image_alpha -= 0.08;

if(image_alpha <= 0){
	instance_destroy();
}

if(image_index >= image_number -1){
	image_speed= 0;
}else{
	image_speed= .3;
}