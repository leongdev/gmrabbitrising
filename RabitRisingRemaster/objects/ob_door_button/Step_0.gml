/// @description Insert description here
// You can write your code in this editor
if(isActivated){
	
	if(activationLock){
		activationLock = false;
		sprite_index = sp_button_deactivated;
		ob_door.sprite_index = sp_door_open;
	}
	if(image_index >= image_number -1){
		image_speed = 0;
	}

	if(ob_door.image_index >= ob_door.image_number -1){
		ob_door.image_speed = 0;
	}
}

